package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const monitoramentos = 3
const delay = 5

func main() {
	exibeIntroducao()
	for {

		exibeMenu()

		comando := leComando()

		switch comando {
		case 1:
			iniciarMonitoramento()
		case 2:
			fmt.Println("Exibindo logs...")
			imprimeLogs()
		case 0:
			fmt.Println("Saindo do programa...")
			os.Exit(0)
		default:
			fmt.Println("Não conheço este comando !!!")
			os.Exit(-1)
		}
	}
}

func exibeIntroducao() {
	nome := "Jorge"
	versao := 1.1

	fmt.Println("Olá Sr. ", nome)
	fmt.Println("Este programa está na versão: ", versao)
}

func leComando() int {
	var comandoLido int

	// fmt.Scanf("%d", &comando)
	fmt.Scan(&comandoLido)

	// fmt.Println("O endereço da variável comando na memória é: ", &comando)
	fmt.Println("O comando escolhiodo foi: ", comandoLido)

	return comandoLido

}

func exibeMenu() {
	fmt.Println("1 - Iniciar Monitoramento")
	fmt.Println("2 - Exibir Logs")
	fmt.Println("0 - Sair do Programa")
}

func iniciarMonitoramento() {
	fmt.Println("Monitorando...")

	sites := lerSitesDoArquivo()

	for i := 0; i < monitoramentos; i++ {
		for i, site := range sites {
			fmt.Println("Testando site ", i, " : ", site)
			testaSite(site)
		}
		time.Sleep(delay * time.Second)
	}
}

func testaSite(site string) {
	resp, error := http.Get(site)
	if error != nil {
		fmt.Println("Ocorreu um erro: ", error)
	}
	if resp.StatusCode == 200 {
		fmt.Println("O site ", site, " foi carregado com sucesso ! :D\n")
		registraLog(site, true)
	} else {
		fmt.Println("O site ", site, " está com problemas : / -- Status Code: ", resp.StatusCode, "\n")
		registraLog(site, false)
	}
}

func lerSitesDoArquivo() []string {
	var sites []string

	arquivo, err := os.Open("/home/jorge-augusto-pinto/Documentos/cursos/golang/go/src/hello/sites.txt")

	if err != nil {
		fmt.Println("Ocorreu um erro: ", err)
	}
	leitor := bufio.NewReader(arquivo)
	for {
		linha, err := leitor.ReadString('\n')
		linha = strings.TrimSpace(linha)

		sites = append(sites, linha)

		if err == io.EOF {
			break
		}
	}
	fmt.Println(sites)

	arquivo.Close()

	return sites
}

func registraLog(site string, status bool) {
	arquivo, err := os.OpenFile("/home/jorge-augusto-pinto/Documentos/cursos/golang/go/src/hello/log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println(err)
	}

	hora := time.Now().Format("02/01/2006 15:04:05")

	arquivo.WriteString(hora + " - " + site + " - online: " + strconv.FormatBool(status) + "\n")
	arquivo.Close()
}

func imprimeLogs() {
	arquivo, err := ioutil.ReadFile("/home/jorge-augusto-pinto/Documentos/cursos/golang/go/src/hello/log.txt")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(arquivo))
}
