Estudo de Go Lang

``` 
pasta-do-usuario/
└── go
    ├── bin
    ├── pkg
    └── src
```

 A bin , onde ficará os compilados do nosso código Go.

A pkg, onde ficará os pacotes compartilhados das nossas aplicações, pois o Go é uma linguagem bastante modular, dependendo bastante de pacotes que vamos importando ao longo de nossos códigos.

A src, onde escreveremos o código fonte de cada aplicação.